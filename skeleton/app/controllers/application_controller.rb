class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  before_action :set_locale

  def set_locale
    I18n.locale = params[:locale] || I18n.default_locale
  end

  def restricted_area
    redirect_to new_user_session_path if !user_signed_in?
    redirect_to new_charge_path if current_user.has_restrict_access?
  end

end
