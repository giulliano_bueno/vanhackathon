json.array!(@skills) do |skill|
  json.extract! skill, :id, :skill_category_id, :name, :date, :score, :level, :rank
  json.url skill_url(skill, format: :json)
end
