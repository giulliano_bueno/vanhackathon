Rails.application.routes.draw do

  resources :skills
  resources :skill_categories
  resources :projects

  resources :jobs
  get 'find/my/job' => 'jobs#find'

  resources :companies
  #devise_for :users

  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  root 'home#index'

  # devise_for :users, controllers: { sessions: "users/sessions" }

  devise_for :users, controllers: { sessions: "users/sessions", :registrations => "users/registrations" } #, path: "auth", path_names: { sign_in: 'login', sign_out: 'logout', password: 'secret', confirmation: 'verification', unlock: 'unblock', registration: 'register', sign_up: 'cmon_let_me_in' }
  devise_scope :user do
    post 'pay', to: 'users/registrations#pay'
  end

  resources :charges

  get 'cart' => 'home#cart'
  get 'contact' => 'home#contact'
  get 'about' => 'home#about'
  get 'blog' => 'home#blog'
  get 'blog/post' => 'home#blog_post'
  get 'invoice' => 'home#invoice'
  get 'premium' => 'home#premium'

  get 'account' => 'account#index', as: :user
  patch 'account/update' => 'account#update'


end
