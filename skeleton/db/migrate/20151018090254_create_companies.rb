class CreateCompanies < ActiveRecord::Migration
  def change
    create_table :companies do |t|
      t.string :name
      t.string :website
      t.string :location
      t.string :markets
      t.string :high_concept_pitch
      t.text :why_us
      t.text :product

      t.timestamps null: false
    end
  end
end
